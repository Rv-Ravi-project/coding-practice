#include<string>
#include<math.h>
#include<iostream>


void lstrip(std::string &str)
{
	int size = str.size();
	int i = 0;
	for (;i < size && str[i] == ' ';i++);
	str = (i < size || i == 0)?str.substr(i):"";
}
bool isDigit(const char& ch )
{
	return (ch >= 48 && ch <= 57);
}

void getStrNum(std::string &str)
{
	std::string tempNum = "";
	int size = str.size();
	for(int i = 0;i < size; i++)
	{
        if((str[i] == '-' || str[i] == '+') && (i + 1 < size && isDigit(str[i + 1])))
            tempNum += str[i];
        else if(isDigit(str[i]))
            tempNum += str[i];
        else
            break;
}
	str = tempNum;

}
int getNum(const std::string &str)
{
	if (!str.empty()){
	try{
            return std::stoi(str);
        }
        catch(std::exception & e)
        {
            if (str[0] == '-')
                return pow(-2,31);
            return pow(2,31) - 1;
        }
	}
	return 0;
}

int main()
{
	std::string str = "  -0012a42";
	lstrip(str);
    getStrNum(str);
    std::cout << getNum(str) << std::endl; 
}
