#include<iostream>
#include<vector>

int maxArea(const std::vector<int>& height) {
    int max = 0,i = 0,j = height.size() - 1,temp = 0;

    while(i < j)
    {
    	if(height[i] < height[j])
    	{
    		temp = height[i] * (j - i);
    		if (max < temp)
    			max = temp;
    		i++;
    	}
    	else
    	{
    		temp = height[j] * (j - i);
    		if (max < temp)
    			max = temp;
    		j--;
    	}

    }
    return max;
}

int main()
{
	std::cout << maxArea({4,3,2,1,4}) << std::endl;; 
	return 0;
}
